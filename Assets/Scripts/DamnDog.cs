﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamnDog : MonoBehaviour
{
    public GameObject brawl;
    public GameObject shit;
    public int shitCooldown;
    bool canShit = true;
    float shitSpawn;

    List<GameObject> inContact;
    
    Player playerScripit;

    // Start is called before the first frame update
    void Start()
    {
        playerScripit = GetComponent<Player>();
        inContact = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DogAction(){
        if (inContact.Count == 0 && canShit){
            shitSpawn = (playerScripit.direction / 3) * -1;
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().ChangeShit(1);
            GameObject myShit = Instantiate(
                shit,
                new Vector2((float)transform.position.x + shitSpawn, transform.position.y),
                transform.rotation);
            Physics2D.IgnoreCollision(myShit.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            DontShitNow();
        }
    }
    
    public void DontShitNow(){
        canShit = false;
        StartCoroutine(ShitCooldown(shitCooldown));
    }

    public void StopCooldown(){
        StopCoroutine("ShitCooldown");
    }

    public void Lost(){
        canShit = false;
    }

    IEnumerator ShitCooldown(int timer){
        while (timer > 0){
            yield return new WaitForSeconds(1);
            timer --;
        }
        canShit = true;
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "shit"){
            inContact.Add(col.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col){
        if (col.tag == "shit"){
            inContact.Remove(col.gameObject);
        }
    }
    
}
