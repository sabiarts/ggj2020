﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Text matchTimerText;
    public Text playerScoreText;
    public Text kekScoreText;

    public GameObject readyScreen;
    
    public float matchTimer;
    public float playerScore;
    public float kekScore;

    public Player player1;
    public Player player2;
    public Player player3;

    public GameObject pausePanel;
    bool isPaused;
    bool ready;

    public GameObject endGamePanel;
    public Image endGameFace;
    public Image endGameFace2;
    public Sprite kekFace;
    public Sprite manFace;
    public Sprite womanFace;
    public Text winText;
    public Text winPoints;

    AudioSource winAudioSource;
    int shitCount = 0;
    bool dogDown = false;

    SceneRandomizer sceneRandomizerScript;

    // Start is called before the first frame update
    void Start()
    {
        sceneRandomizerScript = GetComponent<SceneRandomizer>();
        winAudioSource = GetComponent<AudioSource>();
        startMatch();
        playerScoreText.text = 0 + "pts";
        kekScoreText.text = 0 + "pts";
        Time.timeScale = 0;
        ready = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ready){
            if (Input.GetButtonDown("PauseKey")||Input.GetButtonDown("Pause"))
            {
                if(isPaused){
                    ContinueMatch();
                } else{
                    PauseMatch();
                }
            }
        } else{
            if (Input.GetButtonDown("Pause")||Input.GetButtonDown("PauseKey")||Input.GetButtonDown("Fire1")||Input.GetButtonDown("Fire2")||Input.GetButtonDown("Fire3")||Input.GetButtonDown("Fire1Key")||Input.GetButtonDown("Fire2Key")||Input.GetButtonDown("Fire3Key")){
                ReadyToGo();
            }
        }
        
        matchTimer -= Time.deltaTime;
        if(matchTimer <= 0)
        {
            endMatch();
        }
        int matchTimerInt = (int)matchTimer;
        matchTimerText.text = matchTimerInt.ToString();
    }

    public void ReadyToGo(){
        ContinueMatch();
        readyScreen.SetActive(false);
        ready = true;
        player1.arrowCanvas.SetActive(false);
        player2.arrowCanvas.SetActive(false);
        player3.arrowCanvas.SetActive(false);
    }

    public void startMatch()
    {
        //Player[] playerList = new Player[3] {player1,player2,player3};
        Color32 color1 = new Color32(255,0,0,255);
        Color32 color2 = new Color32(0,255,0,255);
        Color32 color3 = new Color32(0,0,255,255);

        switch ((int)Random.Range(0, 5.99f))
        {
            case 0:
                player1.typeInput = "1";
                player1.arrow.color = color1;
                player2.typeInput = "2";
                player2.arrow.color = color2;
                player3.typeInput = "3";
                player3.arrow.color = color3;
                break;

            case 1:
                player1.typeInput = "1";
                player1.arrow.color = color1;
                player2.typeInput = "3";
                player2.arrow.color = color3;
                player3.typeInput = "2";
                player3.arrow.color = color2;
                break;

            case 2:
                player1.typeInput = "2";
                player1.arrow.color = color2;
                player2.typeInput = "1";
                player2.arrow.color = color1;
                player3.typeInput = "3";
                player3.arrow.color = color3;
                break;

            case 3:
                player1.typeInput = "2";
                player1.arrow.color = color2;
                player2.typeInput = "3";
                player2.arrow.color = color3;
                player3.typeInput = "1";
                player3.arrow.color = color1;
                break;

            case 4:
                player1.typeInput = "3";
                player1.arrow.color = color3;
                player2.typeInput = "1";
                player2.arrow.color = color1;
                player3.typeInput = "2";
                player3.arrow.color = color2;
                break;

            case 5:
                player1.typeInput = "3";
                player1.arrow.color = color3;
                player2.typeInput = "2";
                player2.arrow.color = color2;
                player3.typeInput = "1";
                player3.arrow.color = color1;
                break;
            default:
                Debug.LogError("Unable to set inputs for result");
                break;
        }        
    }
    public void playerMadeAPoint()
    {
        playerScore += 10;
        playerScoreText.text = playerScore + " pts";
    }
    public void kekMadeAPoint()
    {
        kekScore += 1;
        kekScoreText.text = kekScore + " pts";
    }

    public void ChangeShit(int ammount){
        shitCount += ammount;
        if (dogDown){
            if(shitCount == 0){
                endMatch();
            }
        }
    }

    public int GetShit(){
        return shitCount;
    }

    public void DogDown(){
        dogDown = true;
    }

    public void PauseMatch()
    {
        isPaused = true;
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void ContinueMatch()
    {
        Time.timeScale = 1;
        isPaused = false;
        pausePanel.SetActive(false);
    }
    public void TryAgain()
    {
        Time.timeScale = 1;
        sceneRandomizerScript.sceneRandomizer();
    }

    public void exitMatch()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void endMatch()
    {
        Time.timeScale = 0;
        winAudioSource.Play();
        if (kekScore > playerScore)
        {
            winText.text = "Kek made a mess!";
            winPoints.text = "Score: " + kekScore + "pts";
            endGameFace.sprite = kekFace;
        }
        else if(kekScore < playerScore)
        {
            winText.text = "The couple kept the house clean!";
            winPoints.text = "Score: " + playerScore + "pts";
            endGameFace.sprite = womanFace;
            endGameFace2.sprite = manFace;
            endGameFace2.color = new Color (1,1,1,1);
        }
        else if(kekScore == playerScore)
        {
            winText.text = "Draw!";
            winPoints.text = "Score: " + kekScore + "pts";
        }
        endGamePanel.SetActive(true);
    }
}
