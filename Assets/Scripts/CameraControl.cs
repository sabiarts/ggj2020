﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    GameObject player1;
    GameObject player2;
    GameObject kek;
    Transform cameraTr;
    float cameraPosX;
    float cameraPosY;
    public float cameraMinDistance = 4;
    public float cameraMaxDistance = 7;
    public float negativeBlock = -6f;
    public float positiveBlock = 5f;
    

    // Start is called before the first frame update
    void Start()
    {
        player1 = GameObject.FindGameObjectWithTag("Player1");
        player2 = GameObject.FindGameObjectWithTag("Player2");
        kek = GameObject.FindGameObjectWithTag("Kek");
        cameraTr = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Seguir o centro da camera
        //x = (Ax + Bx + Cx) / 3
        cameraPosX = ((player1.transform.position.x + player2.transform.position.x + kek.transform.position.x)/3);
        cameraPosY = ((player1.transform.position.y + player2.transform.position.y + kek.transform.position.y)/3);
        cameraTr.position = new Vector3(cameraPosX, cameraPosY, cameraTr.position.z);
        //Controle distanciamento camera
        #region ControleX
        if ((player1.transform.position.x + cameraTr.position.x) < negativeBlock || (player1.transform.position.x + cameraTr.position.x) > positiveBlock)
        {
            GetTheCameraAway();
        }
        if ((player2.transform.position.x + cameraTr.position.x) < negativeBlock || (player2.transform.position.x + cameraTr.position.x) > positiveBlock)
        {
            GetTheCameraAway();
        }
        if ((kek.transform.position.x + cameraTr.position.x) < negativeBlock || (kek.transform.position.x + cameraTr.position.x) > positiveBlock)
        {
            GetTheCameraAway();
        }
        //Aproximacao camera
        if (((player1.transform.position.x - cameraTr.position.x) > negativeBlock && (player1.transform.position.x + cameraTr.position.x) < positiveBlock) &&
                ((player2.transform.position.x - cameraTr.position.x) > negativeBlock && (player2.transform.position.x + cameraTr.position.x) < positiveBlock) &&
                ((kek.transform.position.x - cameraTr.position.x) > negativeBlock && (kek.transform.position.x + cameraTr.position.x) < positiveBlock))
        {
            GetTheCameraClose();
        }
        #endregion
        #region ControleY
        //distanciamento camera
        if ((player1.transform.position.y + cameraTr.position.y) < negativeBlock || (player1.transform.position.y + cameraTr.position.y) > positiveBlock)
        {
            GetTheCameraAway();
        }
        if ((player2.transform.position.y + cameraTr.position.y) < negativeBlock || (player2.transform.position.y + cameraTr.position.y) > positiveBlock)
        {
            GetTheCameraAway();
        }
        if ((kek.transform.position.y - cameraTr.position.y) < negativeBlock/2 || (kek.transform.position.y + cameraTr.position.y) > positiveBlock/2)
        {
            GetTheCameraAway();
        }
        //Aproximacao camera
        if (((player1.transform.position.y + cameraTr.position.y) > negativeBlock && (player1.transform.position.y - cameraTr.position.y) < positiveBlock) &&
                ((player2.transform.position.y + cameraTr.position.y) > negativeBlock && (player2.transform.position.y - cameraTr.position.y) < positiveBlock) &&
                ((kek.transform.position.y + cameraTr.position.y) > negativeBlock && (kek.transform.position.y - cameraTr.position.y) < positiveBlock))
        {
            GetTheCameraClose();
        }
        #endregion

        void GetTheCameraAway()
        {
            Vector3 cameraLerp = new Vector3(Mathf.Lerp(Camera.main.orthographicSize, cameraMaxDistance, 0.01f), 0, 0);
            float cameraCalculator = cameraLerp.x;
            Camera.main.orthographicSize = cameraCalculator;
        }
        void GetTheCameraClose()
        {
            Vector3 cameraLerp = new Vector3(Mathf.Lerp(Camera.main.orthographicSize, cameraMinDistance, 0.01f), 0, 0);
            float cameraCalculator = cameraLerp.x;
            Camera.main.orthographicSize = cameraCalculator;
        }
    }
}
