﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneRandomizer : MonoBehaviour
{
    public string randomizedScene;
    public void sceneRandomizer()
    {
        switch ((int) Random.Range(0, 3.999f))
        {
            case 0:
                randomizedScene = "Map1";
                break;

            case 1:
                randomizedScene = "Map2";
                break;

            case 2:
                randomizedScene = "Map3";
                break;

            case 3:
                randomizedScene = "Map4";
                break;
        }

        SceneManager.LoadScene(randomizedScene);
    }

    public void quit()
    {
        Application.Quit();
    }
}
