﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TugOfWar : MonoBehaviour
{
    GameObject hotDog;
    GameObject hotBrawler;
    public Image gaugeImage;
    public GameObject gaugeObject;
    public AudioSource audioSource;
    
    [SerializeField]
    int serialTimer;
    int tugOWar;

    GameController gameController;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    public void BrawlStart(GameObject brawler, GameObject dog){
        audioSource.Play();
        hotBrawler = brawler;
        hotDog = dog;
        hotBrawler.GetComponent<SpriteRenderer>().enabled = false;
        hotBrawler.GetComponent<Player>().SetCanMove(false);
        hotBrawler.GetComponent<Player>().GotIntoBrawl();
        hotDog.GetComponent<SpriteRenderer>().enabled = false;
        hotDog.GetComponent<Player>().brawl = gameObject;
        hotDog.GetComponent<Player>().SetCanMove(false);
        hotDog.GetComponent<Player>().GotIntoBrawl();
        tugOWar = 50;
        gaugeObject.SetActive(true);
        StartCoroutine(Brawling(serialTimer));
    }

    void BrawlResult(){
        audioSource.Stop();
        hotBrawler.GetComponent<SpriteRenderer>().enabled = true;
        hotDog.GetComponent<SpriteRenderer>().enabled = true;
        hotDog.GetComponent<Player>().SetCanMove(false);
        hotDog.GetComponent<Player>().BrawlStopped();
        hotBrawler.GetComponent<Player>().BrawlStopped();
        if(tugOWar >=50){
            Debug.Log("Dono espancou cachorro");
            //hotBrawler.GetComponent<Player>().SetCanMove(true);
            hotDog.GetComponent<DamnDog>().Lost();
            hotBrawler.GetComponent<Owner>().animReset();
            if(gameController.GetShit() == 0){
                gameController.endMatch();
            } else{
                gameController.DogDown();
            }
        } else{
            Debug.Log("Kek ganhou a treta");
            hotDog.GetComponent<DamnDog>().DontShitNow();
            hotDog.GetComponent<Player>().SetCanMove(true);
            hotDog.GetComponent<Player>().ReduceTugForce();
            hotBrawler.GetComponent<Owner>().DoStepCooldown();
        }
        hotDog.GetComponent<Player>().brawl = null;
        hotBrawler.GetComponent<Player>().brawl = null;
        gaugeObject.SetActive(false);
        Destroy(gameObject);
    }

    public void Tug(int tugForce){
        tugOWar += tugForce;
        gaugeImage.fillAmount = (float) tugOWar / 100;
        if(tugOWar <= 0){
            tugOWar = 0;
            StopCoroutine("Brawling");
            BrawlResult();
        }
        if(tugOWar >= 100){
            tugOWar = 100;
            StopCoroutine("Brawling");
            BrawlResult();
        }
    }

    IEnumerator Brawling(int timer){
        while (timer > 0){
            yield return new WaitForSeconds(1);
            timer --;
        }
        Debug.Log("Timeout");
        BrawlResult();
    }
}
