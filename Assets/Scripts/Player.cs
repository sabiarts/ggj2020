﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    public string typeInput;
    public float direction;
    public GameObject brawl;
    public GameObject arrowCanvas;
    public Image arrow;

    public int playerType;

    bool canMove;
    bool inBrawl;

    [SerializeField]
    int tugForce;

    Animator playerAnimator;
    SpriteRenderer playerSprite;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        inBrawl = false;
        playerAnimator = GetComponent<Animator>();
        playerSprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(canMove){
            PlayerMove();
        }
        PlayerAction();
    }

    void PlayerMove(){
        // == bloco temporario até a escolha de controles funcionar ==
        if (Input.GetAxisRaw("Horizontal" + typeInput) != 0 || Input.GetAxisRaw("Vertical" + typeInput) != 0){
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(
                moveSpeed * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal" + typeInput),
                moveSpeed * Time.fixedDeltaTime * Input.GetAxisRaw("Vertical" + typeInput));
        } else if(Input.GetAxisRaw("Horizontal" + typeInput + "Key") != 0 || Input.GetAxisRaw("Vertical" + typeInput + "Key") != 0){
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(
                moveSpeed * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal" + typeInput + "Key"),
                moveSpeed * Time.fixedDeltaTime * Input.GetAxisRaw("Vertical" + typeInput + "Key"));
        } else{
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        }
        // == FIM do bloco ==
        // == Animações ajustadas para funcionarem com o teclado também, arrumar quando a seleção de controles for implementada
        if (Input.GetAxisRaw("Horizontal" + typeInput) > 0 || Input.GetAxisRaw("Horizontal" + typeInput + "Key") > 0){
            

            direction = 1;
            playerSprite.flipX = true;
            //Debug.Log("->");
            //setIsMoving(true);
            playerAnimator.SetBool("isRunning", true);

        }
        if (Input.GetAxisRaw("Horizontal" + typeInput) < 0 || Input.GetAxisRaw("Horizontal" + typeInput + "Key") < 0){
            
            direction = -1;
            playerSprite.flipX = false;
            //Debug.Log("<-");
            //setIsMoving(true);
            playerAnimator.SetBool("isRunning", true);
        }
        if (Input.GetAxisRaw("Vertical" + typeInput) != 0 || Input.GetAxisRaw("Vertical" + typeInput + "Key") != 0){
            //Debug.Log("^v");
            //setIsMoving(true);
            playerAnimator.SetBool("isRunning", true);
        }
        if (Input.GetAxisRaw("Horizontal" + typeInput) == 0 && Input.GetAxisRaw("Vertical" + typeInput) == 0 && Input.GetAxisRaw("Horizontal" + typeInput + "Key") == 0 && Input.GetAxisRaw("Vertical" + typeInput + "Key") == 0){
            //Debug.Log("x");
            //setIsMoving(false);
            playerAnimator.SetBool("isRunning", false);
        }
        //SpriteWork();
    }

    void PlayerAction(){
        if (Input.GetButtonDown("Fire" + typeInput) || Input.GetButtonDown("Fire" + typeInput + "Key")){
            if(inBrawl){
                brawl.GetComponent<TugOfWar>().Tug(tugForce);
            } else if(playerType == 0){
                GetComponent<DamnDog>().DogAction();
            } else{
                GetComponent<Owner>().OwnerAction();
            }
        }
    }

    public void SetCanMove(bool newState){
        canMove = newState;
        Debug.Log("Can Move? " + canMove);
        if (!canMove){
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        } else{
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            transform.rotation = new Quaternion(0,0,0,0);
        }
    }

    public void GotIntoBrawl(){
        inBrawl = true;
        if (playerType == 0){
            GetComponent<DamnDog>().StopCooldown();
        }
    }

    public void BrawlStopped(){
        inBrawl = false;
        if (playerType == 0){

        }
    }

    public void ReduceTugForce(){
        tugForce += 2;
    }
}
