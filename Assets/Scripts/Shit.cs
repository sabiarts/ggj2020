﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shit : MonoBehaviour
{
    bool stepped;
    GameController gameController;

    public Sprite steppedSprite;

    void Start()
    {
        stepped = false;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        StartCoroutine(Scoring());
    }

    public bool GetStep(){
        return stepped;
    }

    public void StepOn(){
        stepped = true;
        Debug.Log("Stepped on");
        GetComponent<SpriteRenderer>().sprite = steppedSprite;
    }

    IEnumerator Scoring(){
        while(true){
            yield return new WaitForSeconds(1);
            gameController.kekMadeAPoint();
        }
    }
}
