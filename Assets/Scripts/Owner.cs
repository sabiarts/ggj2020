﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Owner : MonoBehaviour
{
    public Image gaugeImage;
    public GameObject gaugeObject;
    public GameObject brawlPrefab;
    public int stepCooldown;
    int tugOWar;
    bool cleaning = false;
    Player playerScripit;

    AudioSource audioSource;
    public AudioClip slipperyShit;
    public AudioClip sweppingBostas;
    
    [SerializeField]
    GameObject toClean;
    GameController gameController;

    Animator playerAnimator;

    // Start is called before the first frame update
    void Start()
    {
        playerScripit = GetComponent<Player>();
        audioSource = GetComponent<AudioSource>();
        playerAnimator = GetComponent<Animator>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OwnerAction(){
        if(playerScripit.playerType == 1 && !cleaning){
            CleanPath();
        }
    }

    void CleanPath(){
        if(toClean != null){
            playerScripit.SetCanMove(false);
            cleaning = true;
            StartCoroutine(CleanUp(stepCooldown));
        }
    }

    void OnCollisionEnter2D(Collision2D col){
        GameObject collided = col.gameObject;
        Debug.Log("!!!");
        if(collided.tag == "shit"){

            if(collided.GetComponent<Shit>().GetStep() is false && playerScripit.playerType == 2){
                Debug.Log("Aw shit, here we go again");
                playerAnimator.SetBool("isFalling", true);
                audioSource.clip = slipperyShit;
                audioSource.Play();
                StartCoroutine(StepCooldown(stepCooldown));
                collided.GetComponent<Shit>().StepOn();
                playerScripit.SetCanMove(false);
                Physics2D.IgnoreCollision(collided.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            } else if (playerScripit.playerType == 1){
                Debug.Log("LIMPA DIABO");                
                toClean = collided;
            }
        }
        if(collided.tag == "Kek" && playerScripit.playerType == 2){
            Debug.Log("Damn dog, stop bullshitting me");
            CatchDog(collided.gameObject);
        }
    }

    void OnCollisionExit2D(Collision2D col){
        if(col.gameObject.tag == "shit"){
            Debug.Log("Duh");
            toClean = null;
        }
    }

    void CatchDog(GameObject dog){
        GetComponent<Player>().brawl = Instantiate(brawlPrefab, transform.position, transform.rotation);
        GetComponent<Player>().brawl.GetComponent<TugOfWar>().BrawlStart(gameObject, dog);
    }

    public void DoStepCooldown(){
        playerAnimator.SetBool("isFalling", true);
        StartCoroutine(StepCooldown(stepCooldown));
    }

    public IEnumerator StepCooldown(int timer){
        int maxTimer = timer;
        gaugeObject.SetActive(true);
        GetComponent<Collider2D>().enabled = false;
        while (timer > 0){
            //Debug.Log("loading...");
            yield return new WaitForFixedUpdate();
            timer --;
            gaugeImage.fillAmount = (float) timer / maxTimer;
        }
        playerScripit.SetCanMove(true);
        GetComponent<Collider2D>().enabled = true;
        gaugeObject.SetActive(false);
        animReset();
    }

    IEnumerator CleanUp(int timer){
        int maxTimer = timer;
        gaugeObject.SetActive(true);
        playerAnimator.SetBool("isCleaning", true);
        audioSource.clip = sweppingBostas;
        audioSource.Play();
        while (timer > 0){
            //Debug.Log("loading...");
            yield return new WaitForFixedUpdate();
            timer --;
            gaugeImage.fillAmount = (float) timer / maxTimer;
        }
        Destroy(toClean);
        gameController.playerMadeAPoint();
        gameController.ChangeShit(-1);
        toClean = null;
        playerScripit.SetCanMove(true);
        gaugeObject.SetActive(false);
        playerAnimator.SetBool("isCleaning", false);
        cleaning = false;
    }

    public void animReset()
    {
        playerAnimator.SetBool("isFalling", false);
        playerAnimator.SetBool("isRunning", false);
        playerAnimator.SetBool("isCleaning", false);
        playerAnimator.SetBool("isTreta", false);
    }
    
}
